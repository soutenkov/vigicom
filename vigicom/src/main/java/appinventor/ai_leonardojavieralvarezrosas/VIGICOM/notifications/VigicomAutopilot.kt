package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.v4.content.ContextCompat
import android.util.Log
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.R
import com.urbanairship.AirshipConfigOptions
import com.urbanairship.Autopilot
import com.urbanairship.UAirship

@Suppress("unused")
class VigicomAutopilot : Autopilot() {
    override fun onAirshipReady(airship: UAirship?) {
        airship?.pushManager?.userNotificationsEnabled = true
        airship?.pushManager?.pushTokenRegistrationEnabled = true

        if (Build.VERSION.SDK_INT >= 26) {
            val context = UAirship.getApplicationContext()
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val channel = NotificationChannel("Notifications",
                    "Notificaciones",
                    NotificationManager.IMPORTANCE_DEFAULT)

            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun createAirshipConfigOptions(context: Context): AirshipConfigOptions? {

        return AirshipConfigOptions.Builder()
                .setDevelopmentAppKey("ewqJgam0TcGVREgdhf3-JA")
                .setDevelopmentAppSecret("U4GjS1W2SvCUjTA6kZzVLQ")
                .setProductionAppKey("YC5ZBnYKQFSS2uhVr2X7tg")
                .setProductionAppSecret("N3u5n2P3SnGrLt4_3AmKpA")
                .setDevelopmentLogLevel(Log.DEBUG)
                .setProductionLogLevel(Log.ERROR)
                .setInProduction(true)
                .setGcmSender("93340378639")
                .setNotificationIcon(R.mipmap.ic_launcher_round)
                .setNotificationAccentColor(ContextCompat.getColor(context, R.color.colorAccent))
                .setNotificationChannel("Notifications")
                .build()
    }
}