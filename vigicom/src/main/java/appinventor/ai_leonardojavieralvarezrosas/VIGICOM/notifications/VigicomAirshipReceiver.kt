package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.notifications

import android.content.Context
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.Constants
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.defaultPrefs
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.set
import com.urbanairship.AirshipReceiver


class VigicomAirshipReceiver : AirshipReceiver() {
    override fun onChannelCreated(context: Context, channelId: String) {
        val prefs = defaultPrefs(context)
        prefs[Constants.Prefs.URBAN_CHANNEL] = channelId
    }

}