package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils


class Constants {

    class Prefs {
        companion object {
            const val URBAN_CHANNEL = "URBAN_CHANNEL"
            const val PHONE_NUMBER = "PHONE_NUMBER"
            const val REGISTERED = "REGISTERED"
        }
    }

    class Params {
        companion object {
            const val MODE = "mod"
            const val COMMAND = "com"
            const val PHONE = "tel"
            const val TOKEN = "tok"
            const val CODE = "cod"
        }
    }

}