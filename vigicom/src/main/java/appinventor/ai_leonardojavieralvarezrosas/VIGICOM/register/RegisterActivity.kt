package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.register

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.R
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer.NavDrawerActivity
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.service.Commands
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.service.VigicomService
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.content_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("DEPRECATION")
class RegisterActivity : AppCompatActivity() {

    var parentView: View? = null
    var loadingDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = defaultPrefs(this)
        val registered: Boolean? = prefs[Constants.Prefs.REGISTERED, false]
        if (registered!!) navigateToNavDrawerActivity(this)

        setContentView(R.layout.activity_register)
        setSupportActionBar(toolbar)

        register_button.setOnClickListener({ validateAndRegister() })
    }

    private fun validateAndRegister() {
        var canRegister = true
        if (TextUtils.isEmpty(area_code.text) || area_code.text.toString().length < 3) {
            area_code.error = getString(R.string.register_area_code_error)
            canRegister = false
        }
        if (TextUtils.isEmpty(phone_number.text) || phone_number.text.toString().length < 7) {
            phone_number.error = getString(R.string.register_phone_number_error)
            canRegister = false
        }
        if (canRegister) {
            val fullPhoneNumber = "549${area_code.text}${phone_number.text}"

            parentView = findViewById(android.R.id.content)
            loadingDialog = ProgressDialog(this).createLoadingDialog()
            loadingDialog!!.show()

            Commands.REGISTER.params.put(Constants.Params.PHONE, fullPhoneNumber)
            VigicomService.create().sendAction(Commands.REGISTER.params).enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                    loadingDialog?.hide()
                    when (response!!.code()) {
                        200 -> buildVerificationCodeDialog(fullPhoneNumber)
                        else -> {
                            parentView?.snack(R.string.message_request_rejected) {}
                        }
                    }
                }

                override fun onFailure(call: Call<Void>?, t: Throwable?) {
                    loadingDialog?.hide()
                    parentView?.snack(R.string.message_error) {}
                }
            })
        }


    }

    private fun buildVerificationCodeDialog(phoneNumber: String, invalidCode: Boolean = false) {
        val view = View.inflate(this, R.layout.dialog_verification_code, null)
        val codeInput = view.findViewById<EditText>(R.id.code_input)

        val builder = AlertDialog.Builder(this)
        builder.apply {
            setTitle(getString(R.string.register_verification_dialog_title))
            setMessage(getString(R.string.register_verification_dialog_message))
            setView(view)
            setPositiveButton(context.getString(R.string.accept), { _, _ -> })
            setNegativeButton(context.getString(R.string.cancel), { dialog, _ -> dialog.cancel() })
            setCancelable(false)
            if (invalidCode) codeInput.error = getString(R.string.register_verification_code_invalid)
            create().apply {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
                show()
                getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    val code = codeInput?.text.toString().trim()
                    if (code.isEmpty() || code.length < 6) {
                        codeInput.error = getString(R.string.register_verification_code_error)
                    } else {
                        dismiss()
                        loadingDialog?.show()
                        val prefs = defaultPrefs(context)
                        val token: String? = prefs[Constants.Prefs.URBAN_CHANNEL]
                        Commands.VALIDATE.params.apply {
                            put(Constants.Params.PHONE, phoneNumber)
                            put(Constants.Params.CODE, code)
                            if (token != null) put(Constants.Params.TOKEN, token)
                        }
                        VigicomService.create().sendAction(Commands.VALIDATE.params).enqueue(object : Callback<Void> {
                            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                loadingDialog?.hide()
                                when (response!!.code()) {
                                    200 -> {
                                        onValidationSuccess(phoneNumber, context)
                                    }
                                    450 -> {
                                        buildVerificationCodeDialog(phoneNumber, true)
                                    }
                                    else -> {
                                        parentView?.snack(R.string.message_request_rejected) {}
                                    }
                                }
                            }

                            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                loadingDialog?.hide()
                                parentView?.snack(R.string.message_error) {}
                            }
                        })
                    }
                }
            }
        }
    }

    private fun onValidationSuccess(phoneNumber: String, context: Context) {
        val prefs = defaultPrefs(context)
        prefs[Constants.Prefs.PHONE_NUMBER] = phoneNumber
        prefs[Constants.Prefs.REGISTERED] = true
        navigateToNavDrawerActivity(context)
    }

    private fun navigateToNavDrawerActivity(context: Context) {
        val i = Intent(context, NavDrawerActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(i)
        finish()
    }

}
