package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.service

import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.POST
import retrofit2.http.QueryMap

interface VigicomService {
    @POST("app")
    fun sendAction(@QueryMap options: Map<String, String>): Call<Void>

    companion object {
        fun create(): VigicomService {
            val retrofit = Retrofit.Builder()
                    .baseUrl("http://web.vigicom.net.ar:1234/")
                    .client(OkHttpClient().newBuilder().addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BASIC else HttpLoggingInterceptor.Level.NONE
                    }).build())
                    .build()
            return retrofit.create(VigicomService::class.java)
        }
    }
}
