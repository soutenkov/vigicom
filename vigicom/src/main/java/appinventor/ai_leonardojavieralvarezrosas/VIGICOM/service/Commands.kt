package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.service

import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.Constants


enum class Commands(val params: MutableMap<String, String>) {
    ALARM(mutableMapOf(Constants.Params.MODE to "c", Constants.Params.COMMAND to "alarma")),
    ALERT(mutableMapOf(Constants.Params.MODE to "c", Constants.Params.COMMAND to "alerta")),
    HELP(mutableMapOf(Constants.Params.MODE to "c", Constants.Params.COMMAND to "ayuda")),
    TEST(mutableMapOf(Constants.Params.MODE to "c", Constants.Params.COMMAND to "probar")),
    SIRENS_ON(mutableMapOf(Constants.Params.MODE to "c", Constants.Params.COMMAND to "sirenas")),
    LIGHTS_ON(mutableMapOf(Constants.Params.MODE to "c", Constants.Params.COMMAND to "luces")),
    LIGHTS_OFF(mutableMapOf(Constants.Params.MODE to "c", Constants.Params.COMMAND to "apagar_luces")),
    SIRENS_OFF(mutableMapOf(Constants.Params.MODE to "c", Constants.Params.COMMAND to "apagar_sirenas")),
    REGISTER(mutableMapOf(Constants.Params.MODE to "s")),
    VALIDATE(mutableMapOf(Constants.Params.MODE to "v"))
}

