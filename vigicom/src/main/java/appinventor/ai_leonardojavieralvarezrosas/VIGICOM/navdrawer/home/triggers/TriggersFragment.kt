package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer.home.triggers

import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.R
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.service.Commands
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.service.VigicomService
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.*
import kotlinx.android.synthetic.main.fragment_triggers.*
import permissions.dispatcher.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


@Suppress("DEPRECATION")
@RuntimePermissions
class TriggersFragment : Fragment() {

    private val vigicomService by lazy {
        VigicomService.create()
    }

    private val phoneNumber: String? by lazy {
        val prefs = defaultPrefs(context)
        val phoneNumber: String? = prefs[Constants.Prefs.PHONE_NUMBER]
        phoneNumber
    }

    private val token: String? by lazy {
        val prefs = defaultPrefs(context)
        val token: String? = prefs[Constants.Prefs.URBAN_CHANNEL]
        token
    }

    var loadingDialog: ProgressDialog? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_triggers, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingDialog = ProgressDialog(activity).createLoadingDialog()
        alarm_cardview.setOnClickListener {
            val alertBuilder = AlertDialog.Builder(activity)
            alertBuilder.setTitle(getString(R.string.activate_tile) + ' ' + getString(R.string.alarm_tile))
                    .setMessage(R.string.alarm_dialog_message)
                    .setPositiveButton(R.string.accept, { _, _ ->
                        if (!isOnline(context)) {
                            sendSmsWithPermissionCheck(Commands.ALARM.params[Constants.Params.COMMAND]!!)
                        } else {
                            loadingDialog?.show()
                            Commands.ALARM.params.put(Constants.Params.PHONE, phoneNumber!!)
                            Commands.ALARM.params.put(Constants.Params.TOKEN, token!!)
                            vigicomService.sendAction(Commands.ALARM.params).enqueue(object : Callback<Void> {
                                override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                    loadingDialog?.dismiss()
                                    when (response?.code()) {
                                        200 -> getView()?.let { it -> Snackbar.make(it, R.string.command_success, Snackbar.LENGTH_LONG).show() }
                                        450 -> getView()?.let { it -> Snackbar.make(it, R.string.command_error, Snackbar.LENGTH_LONG).show() }
                                        else -> {
                                            sendSmsWithPermissionCheck(Commands.ALARM.params[Constants.Params.COMMAND]!!)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                    loadingDialog?.dismiss()
                                    sendSmsWithPermissionCheck(Commands.ALARM.params[Constants.Params.COMMAND]!!)
                                }

                            })
                        }
                    })
                    .setNegativeButton(R.string.cancel, { dialog, _ ->
                        dialog.dismiss()
                    })
                    .show()
        }

        alert_cardview.setOnClickListener {
            val alertBuilder = AlertDialog.Builder(activity)
            alertBuilder.setTitle(getString(R.string.announce_tile) + ' ' + getString(R.string.alert_tile))
                    .setMessage(R.string.alert_dialog_message)
                    .setPositiveButton(R.string.accept, { _, _ ->
                        if (!isOnline(context)) {
                            sendSmsWithPermissionCheck(Commands.ALERT.params[Constants.Params.COMMAND]!!)
                        } else {
                            loadingDialog?.show()
                            Commands.ALERT.params.put(Constants.Params.PHONE, phoneNumber!!)
                            Commands.ALERT.params.put(Constants.Params.TOKEN, token!!)
                            vigicomService.sendAction(Commands.ALERT.params).enqueue(object : Callback<Void> {
                                override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                    loadingDialog?.dismiss()
                                    when (response?.code()) {
                                        200 -> view?.let { Snackbar.make(it, R.string.command_success, Snackbar.LENGTH_SHORT).show() }
                                        450 -> getView()?.let { it -> Snackbar.make(it, R.string.command_error, Snackbar.LENGTH_LONG).show() }
                                        else -> {
                                            sendSmsWithPermissionCheck(Commands.ALERT.params[Constants.Params.COMMAND]!!)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                    loadingDialog?.dismiss()
                                    sendSmsWithPermissionCheck(Commands.ALERT.params[Constants.Params.COMMAND]!!)
                                }

                            })
                        }
                    })
                    .setNegativeButton(R.string.cancel, { dialog, _ ->
                        dialog.dismiss()
                    })
                    .show()
        }

        help_cardview.setOnClickListener {
            val alertBuilder = AlertDialog.Builder(activity)
            alertBuilder.setTitle(getString(R.string.request_tile) + ' ' + getString(R.string.help_tile))
                    .setMessage(R.string.help_dialog_message)
                    .setPositiveButton(R.string.accept, { _, _ ->
                        if (!isOnline(context)) {
                            sendSmsWithPermissionCheck(Commands.HELP.params[Constants.Params.COMMAND]!!)
                        } else {
                            loadingDialog?.show()
                            Commands.HELP.params.put(Constants.Params.PHONE, phoneNumber!!)
                            Commands.HELP.params.put(Constants.Params.TOKEN, token!!)
                            vigicomService.sendAction(Commands.HELP.params).enqueue(object : Callback<Void> {
                                override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                    loadingDialog?.dismiss()
                                    when (response?.code()) {
                                        200 -> view?.let { Snackbar.make(it, R.string.command_success, Snackbar.LENGTH_SHORT).show() }
                                        450 -> getView()?.let { it -> Snackbar.make(it, R.string.command_error, Snackbar.LENGTH_LONG).show() }
                                        else -> {
                                            sendSmsWithPermissionCheck(Commands.HELP.params[Constants.Params.COMMAND]!!)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                    loadingDialog?.dismiss()
                                    sendSmsWithPermissionCheck(Commands.ALERT.params[Constants.Params.COMMAND]!!)
                                }

                            })
                        }
                    })
                    .setNegativeButton(R.string.cancel, { dialog, _ ->
                        dialog.dismiss()
                    })
                    .show()
        }

        test_cardview.setOnClickListener {
            val alertBuilder = AlertDialog.Builder(activity)
            alertBuilder.setTitle(getString(R.string.system_tile) + ' ' + getString(R.string.test_tile))
                    .setMessage(R.string.test_dialog_message)
                    .setPositiveButton(R.string.accept, { _, _ ->
                        if (!isOnline(context)) {
                            sendSmsWithPermissionCheck(Commands.TEST.params[Constants.Params.COMMAND]!!)
                        } else {
                            loadingDialog?.show()
                            Commands.TEST.params.put(Constants.Params.PHONE, phoneNumber!!)
                            Commands.TEST.params.put(Constants.Params.TOKEN, token!!)
                            vigicomService.sendAction(Commands.TEST.params).enqueue(object : Callback<Void> {
                                override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                    loadingDialog?.dismiss()
                                    when (response?.code()) {
                                        200 -> view?.let { Snackbar.make(it, R.string.command_success, Snackbar.LENGTH_SHORT).show() }
                                        450 -> getView()?.let { it -> Snackbar.make(it, R.string.command_error, Snackbar.LENGTH_LONG).show() }
                                        else -> {
                                            sendSmsWithPermissionCheck(Commands.TEST.params[Constants.Params.COMMAND]!!)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                    loadingDialog?.dismiss()
                                    sendSmsWithPermissionCheck(Commands.TEST.params[Constants.Params.COMMAND]!!)
                                }

                            })
                        }
                    })
                    .setNegativeButton(R.string.cancel, { dialog, _ ->
                        dialog.dismiss()
                    })
                    .show()
        }

    }

    @NeedsPermission(Manifest.permission.SEND_SMS)
    fun sendSms(text: String) {
        sendTextSms(text)
        view?.let { Snackbar.make(it, "Comando enviado por SMS", Snackbar.LENGTH_LONG).show() }
    }

    @OnPermissionDenied(Manifest.permission.SEND_SMS)
    fun onSmsDenied() {
        view?.let { Snackbar.make(it, "Permiso denegado, si no otorga permiso no se podrán enviar SMS en caso de no tener conexión", Snackbar.LENGTH_LONG).show() }
    }

    @OnNeverAskAgain(Manifest.permission.SEND_SMS)
    fun onSmsNeverAskAgain() {
        view?.let { Snackbar.make(it, "Permiso denegado definitivamente, no se podrán enviar SMS en caso de no tener conexión", Snackbar.LENGTH_LONG).show() }
    }

    @OnShowRationale(Manifest.permission.SEND_SMS)
    fun showRationaleForSms(request: PermissionRequest) {
        AlertDialog.Builder(context)
                .setMessage("No hay conexión, se necesita su permiso para enviar SMS")
                .setPositiveButton("Ok", { dialog, which -> request.proceed() })
                .show()
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }
}
