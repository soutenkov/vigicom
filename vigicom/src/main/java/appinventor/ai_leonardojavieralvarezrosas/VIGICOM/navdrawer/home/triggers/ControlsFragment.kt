package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer.home.triggers

import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.R
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.service.Commands
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.service.VigicomService
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.*
import kotlinx.android.synthetic.main.fragment_controls.*
import permissions.dispatcher.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("DEPRECATION")
@RuntimePermissions
class ControlsFragment : Fragment() {

    private val vigicomService by lazy {
        VigicomService.create()
    }

    private val phoneNumber: String? by lazy {
        val prefs = defaultPrefs(context)
        val phoneNumber: String? = prefs[Constants.Prefs.PHONE_NUMBER]
        phoneNumber
    }

    private val token: String? by lazy {
        val prefs = defaultPrefs(context)
        val token: String? = prefs[Constants.Prefs.URBAN_CHANNEL]
        token
    }

    var loadingDialog: ProgressDialog? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_controls, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingDialog = ProgressDialog(activity).createLoadingDialog()
        lights_on_cardview.setOnClickListener {
            val alertBuilder = AlertDialog.Builder(activity)
            alertBuilder.setTitle(getString(R.string.turn_on) + ' ' + getString(R.string.lights))
                    .setMessage(R.string.lights_on_dialog_message)
                    .setPositiveButton(R.string.accept, { _, _ ->
                        if (!isOnline(context)) {
                            sendSmsWithPermissionCheck(Commands.LIGHTS_ON.params[Constants.Params.COMMAND]!!)
                        } else {
                            loadingDialog?.show()
                            Commands.LIGHTS_ON.params.put(Constants.Params.PHONE, phoneNumber!!)
                            Commands.LIGHTS_ON.params.put(Constants.Params.TOKEN, token!!)
                            vigicomService.sendAction(Commands.LIGHTS_ON.params).enqueue(object : Callback<Void> {
                                override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                    loadingDialog?.dismiss()
                                    when (response?.code()) {
                                        200 -> view?.let { Snackbar.make(it, R.string.command_success, Snackbar.LENGTH_SHORT).show() }
                                        450 -> view?.let { it -> Snackbar.make(it, R.string.command_error, Snackbar.LENGTH_LONG).show() }
                                        else -> {
                                            sendSmsWithPermissionCheck(Commands.LIGHTS_ON.params[Constants.Params.COMMAND]!!)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                    loadingDialog?.dismiss()
                                    sendSmsWithPermissionCheck(Commands.LIGHTS_ON.params[Constants.Params.COMMAND]!!)
                                }

                            })
                        }
                    })
                    .setNegativeButton(R.string.cancel, { dialog, _ ->
                        dialog.dismiss()
                    })
                    .show()
        }

        sirens_on_cardview.setOnClickListener {
            val alertBuilder = AlertDialog.Builder(activity)
            alertBuilder.setTitle(getString(R.string.turn_on) + ' ' + getString(R.string.sirens))
                    .setMessage(R.string.sirens_on_dialog_message)
                    .setPositiveButton(R.string.accept, { _, _ ->
                        if (!isOnline(context)) {
                            sendSmsWithPermissionCheck(Commands.SIRENS_ON.params[Constants.Params.COMMAND]!!)
                        } else {
                            loadingDialog?.show()
                            Commands.SIRENS_ON.params.put(Constants.Params.PHONE, phoneNumber!!)
                            Commands.SIRENS_ON.params.put(Constants.Params.TOKEN, token!!)
                            vigicomService.sendAction(Commands.SIRENS_ON.params).enqueue(object : Callback<Void> {
                                override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                                    loadingDialog?.dismiss()
                                    when (response?.code()) {
                                        200 -> view?.let { Snackbar.make(it, R.string.command_success, Snackbar.LENGTH_SHORT).show() }
                                        450 -> view?.let { it -> Snackbar.make(it, R.string.command_error, Snackbar.LENGTH_LONG).show() }
                                        else -> {
                                            sendSmsWithPermissionCheck(Commands.SIRENS_ON.params[Constants.Params.COMMAND]!!)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<Void>?, t: Throwable?) {
                                    loadingDialog?.dismiss()
                                    sendSmsWithPermissionCheck(Commands.SIRENS_ON.params[Constants.Params.COMMAND]!!)
                                }

                            })
                        }
                    })
                    .setNegativeButton(R.string.cancel, { dialog, _ ->
                        dialog.dismiss()
                    })
                    .show()
        }

        lights_off_cardview.setOnClickListener {
            if (!isOnline(context)) {
                sendSmsWithPermissionCheck(Commands.LIGHTS_OFF.params[Constants.Params.COMMAND]!!)
            } else {
                loadingDialog?.show()
                Commands.LIGHTS_OFF.params.put(Constants.Params.PHONE, phoneNumber!!)
                Commands.LIGHTS_OFF.params.put(Constants.Params.TOKEN, token!!)
                vigicomService.sendAction(Commands.LIGHTS_OFF.params).enqueue(object : Callback<Void> {
                    override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                        loadingDialog?.dismiss()
                        when (response?.code()) {
                            200 -> view?.let { Snackbar.make(it, R.string.command_success, Snackbar.LENGTH_SHORT).show() }
                            450 -> view?.let { it -> Snackbar.make(it, R.string.command_error, Snackbar.LENGTH_LONG).show() }
                            else -> {
                                sendSmsWithPermissionCheck(Commands.LIGHTS_OFF.params[Constants.Params.COMMAND]!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<Void>?, t: Throwable?) {
                        loadingDialog?.dismiss()
                        sendSmsWithPermissionCheck(Commands.LIGHTS_OFF.params[Constants.Params.COMMAND]!!)
                    }

                })
            }
        }

        sirens_off_cardview.setOnClickListener {
            if (!isOnline(context)) {
                sendSmsWithPermissionCheck(Commands.SIRENS_OFF.params[Constants.Params.COMMAND]!!)
            } else {
                loadingDialog?.show()
                Commands.SIRENS_OFF.params.put(Constants.Params.PHONE, phoneNumber!!)
                Commands.SIRENS_OFF.params.put(Constants.Params.TOKEN, token!!)
                vigicomService.sendAction(Commands.SIRENS_OFF.params).enqueue(object : Callback<Void> {
                    override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                        loadingDialog?.dismiss()
                        when (response?.code()) {
                            200 -> view?.let { Snackbar.make(it, R.string.command_success, Snackbar.LENGTH_SHORT).show() }
                            450 -> view?.let { it -> Snackbar.make(it, R.string.command_error, Snackbar.LENGTH_LONG).show() }
                            else -> {
                                sendSmsWithPermissionCheck(Commands.SIRENS_OFF.params[Constants.Params.COMMAND]!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<Void>?, t: Throwable?) {
                        loadingDialog?.dismiss()
                        sendSmsWithPermissionCheck(Commands.SIRENS_OFF.params[Constants.Params.COMMAND]!!)
                    }

                })
            }
        }

    }

    @NeedsPermission(Manifest.permission.SEND_SMS)
    fun sendSms(text: String) {
        sendTextSms(text)
        view?.let { Snackbar.make(it, "Comando enviado por SMS", Snackbar.LENGTH_LONG).show() }
    }

    @OnPermissionDenied(Manifest.permission.SEND_SMS)
    fun onSmsDenied() {
        view?.let { Snackbar.make(it, "Permiso denegado, si no otorga permiso no se podrán enviar SMS en caso de no tener conexión", Snackbar.LENGTH_LONG).show() }
    }

    @OnNeverAskAgain(Manifest.permission.SEND_SMS)
    fun onSmsNeverAskAgain() {
        view?.let { Snackbar.make(it, "Permiso denegado definitivamente, no se podrán enviar SMS en caso de no tener conexión", Snackbar.LENGTH_LONG).show() }
    }

    @OnShowRationale(Manifest.permission.SEND_SMS)
    fun showRationaleForSms(request: PermissionRequest) {
        AlertDialog.Builder(context)
                .setMessage("No hay conexión, se necesita su permiso para enviar SMS")
                .setPositiveButton("Ok", { dialog, which -> request.proceed() })
                .show()
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

}
