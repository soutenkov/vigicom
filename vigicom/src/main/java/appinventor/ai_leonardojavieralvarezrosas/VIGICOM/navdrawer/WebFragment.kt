package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer


import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.R
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.isOnline
import im.delight.android.webview.AdvancedWebView
import kotlinx.android.synthetic.main.fragment_web_view.*


class WebFragment : Fragment(), AdvancedWebView.Listener {

    override fun onPageFinished(url: String?) {
        loading?.visibility = View.GONE
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        loading?.visibility = View.GONE
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {

    }

    override fun onExternalPageRequest(url: String?) {

    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
        loading?.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        webview?.onResume()
    }

    override fun onPause() {
        webview?.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        webview?.onDestroy()
        super.onDestroy()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_web_view, container, false)
    }


    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webview.setListener(activity, this)
        if (isOnline(context)) {
            val url = arguments.getString(URL_PARAMETER_NAME)
            webview.loadUrl(url)
        } else {
            no_connection.visibility = View.VISIBLE
        }
    }

    companion object {

        val URL_PARAMETER_NAME = "url"
        fun newInstance(url: String): WebFragment {

            val args = Bundle()
            args.putString(URL_PARAMETER_NAME, url)
            val fragment = WebFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
