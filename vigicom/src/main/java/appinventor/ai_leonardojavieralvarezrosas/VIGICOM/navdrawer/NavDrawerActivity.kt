package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.R
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer.home.TabFragment
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.Constants
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.defaultPrefs
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.get
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.inTransaction
import kotlinx.android.synthetic.main.activity_nav_drawer.*
import kotlinx.android.synthetic.main.app_bar_nav_drawer.*
import okhttp3.HttpUrl
import permissions.dispatcher.*


@RuntimePermissions
class NavDrawerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val phoneNumber: String? by lazy {
        val prefs = defaultPrefs(this)
        val phoneNumber: String? = prefs[Constants.Prefs.PHONE_NUMBER]
        phoneNumber
    }

    private val token: String? by lazy {
        val prefs = defaultPrefs(this)
        val token: String? = prefs[Constants.Prefs.URBAN_CHANNEL]
        token
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_drawer)
        setSupportActionBar(toolbar)

        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            AlertDialog.Builder(this)
                    .setMessage("Cuando no haya conexión, enviaremos los comandos por SMS, con lo cuál necesitamos de su permiso")
                    .setPositiveButton("Ok", { _, _ -> askSmsWithPermissionCheck() })
                    .show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        navigateToScreen(TabFragment(), R.string.home_nav)
        nav_view.setCheckedItem(R.id.nav_home)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                navigateToScreen(TabFragment(), R.string.home_nav)
            }
            R.id.nav_notifications -> {
                navigateToScreen(WebFragment.newInstance(addParameters("http://web.vigicom.net.ar:1234/appAndNotificaciones")), R.string.notifications_nav)
            }
            R.id.nav_chat -> {
                navigateToScreen(WebFragment.newInstance(addParameters("http://web.vigicom.net.ar:1234/appAndChat")), R.string.chat_nav)
            }
            R.id.nav_my_account -> {
                navigateToScreen(WebFragment.newInstance(addParameters("http://web.vigicom.net.ar:1234/appAndCuenta")), R.string.my_account_nav)
            }
            R.id.nav_configuration -> {
                navigateToScreen(WebFragment.newInstance(addParameters("http://web.vigicom.net.ar:1234/appAndConfiguracion")), R.string.configuration_nav)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun navigateToScreen(fragment: Fragment, title: Int) {
        supportFragmentManager.inTransaction {
            replace(R.id.fragments_container, fragment)
        }
        setActionBarTitle(getString(title))
    }

    private fun setActionBarTitle(text: String) {
        supportActionBar?.title = text
    }

    private fun addParameters(baseUrl: String): String {
        val url = HttpUrl.parse(baseUrl)
        val builder = url?.newBuilder()
        val result = builder!!.apply {
            addQueryParameter(Constants.Params.TOKEN, token)
            addQueryParameter(Constants.Params.PHONE, phoneNumber)
            build()
        }
        return result.toString()

    }

    @NeedsPermission(Manifest.permission.SEND_SMS)
    fun askSms() {
        findViewById<View>(android.R.id.content).let { Snackbar.make(it, "Permiso para enviar SMS otorgado", Snackbar.LENGTH_LONG).show() }
    }

    @OnPermissionDenied(Manifest.permission.SEND_SMS)
    fun onSmsDenied() {
        findViewById<View>(android.R.id.content).let { Snackbar.make(it, "Permiso denegado, si no otorga permiso no se podrán enviar SMS en caso de no tener conexión", Snackbar.LENGTH_LONG).show() }
    }

    @OnNeverAskAgain(Manifest.permission.SEND_SMS)
    fun onSmsNeverAskAgain() {
        findViewById<View>(android.R.id.content).let { Snackbar.make(it, "Permiso denegado definitivamente, no se podrán enviar SMS en caso de no tener conexión", Snackbar.LENGTH_LONG).show() }
    }

    @OnShowRationale(Manifest.permission.SEND_SMS)
    fun showRationaleForSms(request: PermissionRequest) {
        AlertDialog.Builder(this)
                .setMessage("Cuando no haya conexión, enviaremos los comandos por SMS. Necesitamos tu permiso")
                .setPositiveButton("Ok", { _, _ -> request.proceed() })
                .show()
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }
}
