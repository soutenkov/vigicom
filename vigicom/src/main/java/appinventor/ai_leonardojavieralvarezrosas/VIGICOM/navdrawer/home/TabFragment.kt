package appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.R
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.Constants
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.defaultPrefs
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.utils.get
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer.WebFragment
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer.home.triggers.ControlsFragment
import appinventor.ai_leonardojavieralvarezrosas.VIGICOM.navdrawer.home.triggers.TriggersFragment
import kotlinx.android.synthetic.main.fragment_tab.*
import okhttp3.HttpUrl

class TabFragment : Fragment() {

    private val phoneNumber: String? by lazy {
        val prefs = defaultPrefs(context)
        val phoneNumber: String? = prefs[Constants.Prefs.PHONE_NUMBER]
        phoneNumber
    }

    private val token: String? by lazy {
        val prefs = defaultPrefs(context)
        val token: String? = prefs[Constants.Prefs.URBAN_CHANNEL]
        token
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_tab, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        tabs.setupWithViewPager(setupViewPager(viewpager))
    }

    private fun setupViewPager(pager: ViewPager): ViewPager {
        val adapter = PageAdapter(activity.supportFragmentManager)
        adapter.addFragment(TriggersFragment(), getString(R.string.triggers_tab))
        adapter.addFragment(ControlsFragment(), getString(R.string.controls_tab))
        adapter.addFragment(WebFragment.newInstance(addParameters("http://web.vigicom.net.ar:1234/appAndInformacion")), getString(R.string.information_tab))
        pager.adapter = adapter
        return pager
    }

    class PageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        private val fragments: MutableList<Fragment> = mutableListOf()
        private val titles: MutableList<String> = mutableListOf()

        override fun getItem(position: Int): Fragment? = fragments[position]

        override fun getCount(): Int = fragments.size

        override fun getPageTitle(position: Int): CharSequence = titles[position]

        fun addFragment(fragment: Fragment, title: String) {
            fragments.add(fragment)
            titles.add(title)
        }
    }

    private fun addParameters(baseUrl: String): String {
        val url = HttpUrl.parse(baseUrl)
        val builder = url?.newBuilder()
        val result = builder!!.apply {
            addQueryParameter(Constants.Params.TOKEN, token)
            addQueryParameter(Constants.Params.PHONE, phoneNumber)
            build()
        }
        return result.toString()

    }
}
